package az.ingress.car.repository;

import az.ingress.car.model.Car;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.util.List;

public interface CarRepository extends JpaRepository<Car, Integer> {
    List<Car> findByModel(String model);

    List<Car> findCarsByAmountGreaterThanAndNumberOfWheels(BigDecimal amount, Integer numberOfWheels);


    @Query(
            nativeQuery = true,
            value
                    = "select * from car a where a.price=:carPrice")
    List<Car> findByPrice(@Param("carPrice") BigDecimal carPrice);

    @Query(nativeQuery = true, value="select count(id) from car")
    Integer countCars();

    @Query(
            nativeQuery = true,
            value
                    = "select * from car where is_electric=?1")
    List<Car> findByIsElectricNative(Boolean isElectric);


}
